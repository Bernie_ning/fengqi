/**
 * Created by wangshuyi on 2017/2/15.
 */

'use strict';

const gulp = require('gulp'),
    sass = require("gulp-sass");
const babel = require("gulp-babel");
const uglify = require("gulp-uglify");

gulp.task('sass', function () {
    return gulp.src('./sass/*')
        .pipe(sass().on('error', sass.logError))
        // .pipe(concat('main.css'))    //合并所有css到main.css
        // .pipe(rename({suffix: '.min'}))   //rename压缩后的文件名
        // .pipe(cssnano())
        .pipe(gulp.dest('../public/custom/css'));
});

gulp.task("pageScript", function () {
    return gulp.src("./page-script/**/*.js")// ES6 源码存放的路径
        .pipe(babel({
            presets: ['es2015']
        }))
        // .pipe(uglify())
        .pipe(gulp.dest("../public/page-script/")); //转换成 ES5 存放的路径
});
gulp.task("customerScript", function () {
    return gulp.src("./customer-script/**/*.js")// ES6 源码存放的路径
        .pipe(babel({
            presets: ['es2015']
        }))
        // .pipe(uglify())
        .pipe(gulp.dest("../public/custom/js/")); //转换成 ES5 存放的路径
});

gulp.task('watch', function () {
    gulp.watch('./sass/*.scss', ['sass']);
    gulp.watch('./page-script/**/*.js', ['pageScript']);
    gulp.watch('./customer-script/**/*.js', ['customerScript']);
});

gulp.task('default', function() {
    // 将你的默认的任务代码放在这
    gulp.start('sass', 'pageScript', 'customerScript', 'watch');
});