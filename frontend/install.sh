#!/usr/bin/env bash
cd ..
npm install --global gulp
npm install --save-dev gulp
npm install --save-dev gulp-sass
npm install --save-dev gulp-concat
npm install --save-dev gulp-uglify
npm install --save-dev gulp-babel