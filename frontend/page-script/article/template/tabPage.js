let pS=20, pN=1;
$('#page-infinite-navbar').infinite(60);
var loading = false;  //状态标记

$(document).scroll(
    function() {
        console.log($(document).scrollTop() + window.innerHeight - $(document).height());
        if ($(document).scrollTop() + window.innerHeight-  $(document).height() >= 0) {
            $('#pastLoad').show();
            if(loading){
                $('#pastLoad').hide();
                return;
            }else{
                loading = true;
                Dolphin.ajax({
                    url:Dolphin.path.contextPath+'/fengqi/article/tabPastForPage',
                    data:{pageSize:pS,pageNumber:pN,bookcase:$('#bookcase').val()},
                    onSuccess:function (reData) {
                        $('#pastLoad').hide();
                        reData = reData.data.rows;
                        pN++;let filePath,eft,dat;
                        console.log(reData);
                        loading = false;
                        if(reData.length!=0){
                            for(let i=0;i<reData.length;i++){
                                if(reData[i].img){
                                    filePath = reData[i].img.filePath
                                }
                                if(reData[i].effectiveTime){
                                    eft = Dolphin.date2string(new Date(reData[i].effectiveTime),'yyyy/MM/dd')
                                }else{
                                    eft = '未规定时间'
                                }

                                if(reData[i].disActiveTime){
                                    dat = Dolphin.date2string(new Date(reData[i].disActiveTime),'yyyy/MM/dd')
                                }else{
                                    dat = '未规定时间'
                                }
                                $('.tab-news-container').append(`
                         <div class="tab-news-list">
                            <a href="${Dolphin.path.contextPath}/fengqi/article/articleView/${reData[i].code }">
                                <div class="tab-news-panel weui-flex">
                                    <div class=" tab-news-img">
                                        <img src="${Dolphin.path.contextPath}/uploadFiles${filePath}"
                                             alt="">
                                    </div>
                                    <div class="tab-news-title ">
                                        <p class="tab-title-font">${reData[i].name}</p>
                                        <span class="tab-title-time">
                                            <span class="icon fa fa-map-marker"></span>
                                            ${reData[i].author|| '地点未定' }
                                            <br><span class="icon fa fa-clock-o"></span>
                                            ${eft}-${dat}
                                    </div>
                                </div>
                            </a>
                        </div>`)
                            }
                        }else{
                            loading = true;
                          $('#haveAll').html('没有更多新闻了...')
                        }

                    }
                })
            }

        }
    });
