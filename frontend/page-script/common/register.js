/**
 * Created by Shubert.Wang on 2016/1/22.
 */

$(function () {
    $('#loginForm').bind("submit", function (e) {
        var data = Dolphin.form.getValue('loginForm');
        var flag = true;
        if(!data.code){
            Dolphin.alert('请填写登录名称');
            flag = false;
        }else if(!data.password){
            Dolphin.alert('请填写登录密码');
            flag = false;
        }else if(data.password.length < 6){
            Dolphin.alert('密码长度不可少于6位');
            flag = false;
        }

        if(flag){
            $.ajax({
                url : contextData.contextPath+'/login/register',
                type : 'post',
                data : data,
                success : function (data) {
                    if(data.success){
                        location.href = Dolphin.path.contextPath + $('#redirectUrl').val();
                    }else{
                        alert(data.message);
                    }
                }
            });
        }
        e.stopPropagation();
        return false;
    });
});