/**
* Created by wangshuyi on 12/1/2017, 2:47:55 PM.
*/

'use strict';
const page = {
    editForm : $('#edit-form'),
    detailForm : $('#detail-form'),
    logForm : $('#log-form'),
    queryConditionForm: $('#queryConditionForm'),

    url : {
        list : '/system/tool/job/list',
        remove : '/system/tool/job/remove/{id}',
        save : '/system/tool/job/save/{id}',
        start : '/system/tool/job/start/{id}',
        stop : '/system/tool/job/stop/{id}',
        run : '/system/tool/job/run/{id}',
        detail : '/system/tool/job/detail/{id}',
        importData : Dolphin.path.contextPath + '/system/tool/job/import',
        exportData : '/system/tool/job/export',

        log: {
            list : '/system/tool/jobLog/list',
        }
    },

    _id : null,
    list : null,
    logList : null,
    editModal : null,
    detailModal : null,
    logModal : null,

    init: null,
    initElement: null,
    initEvent: null,
    showDetail: null,
    formatterDate: null,
};

page.init = function () {
    page.initElement();
    page.initEvent();
};

page.initElement = function () {
    const thisPage = this;
    Dolphin.form.parse();

    thisPage.list = new Dolphin.LIST({
        panel : "#datalist",
        url : thisPage.url.list,
        multiple: false,
        title : "定时任务列表",
        queryParams : Dolphin.form.getValue('queryConditionForm'),
        columns : [{
            code: "name",
            title : "名称",
        },{
            code: "func",
            title : "任务方法",
        },{
            code: "schedule",
            title : "调度时间",
        },{
            code: "status",
            title : "任务状态",
            formatter: function (val) {
                return Dolphin.enum.getEnumText('JobStatus', val);
            }
        },{
            code: "_id",
            title : " ",
            formatter: function (val, row) {
                let content = $('<div>');

                if(row.status === 'stopped'){
                    $('<span class="glyphicon glyphicon-play iconButton">').click(function(e){
                        e.stopPropagation();
                        Dolphin.ajax({
                            url : thisPage.url.start,
                            pathData : {id : val},
                            onSuccess : function (reData) {
                                thisPage.list.reload();
                            }
                        });
                        return false;
                    }).appendTo(content);
                }
                if(row.status === 'running'){
                    $('<span class="glyphicon glyphicon-stop iconButton">').click(function(e){
                        e.stopPropagation();
                        Dolphin.ajax({
                            url : thisPage.url.stop,
                            pathData : {id : val},
                            onSuccess : function (reData) {
                                thisPage.list.reload();
                            }
                        });
                        return false;
                    }).appendTo(content);
                }

                $('<span class="glyphicon glyphicon-refresh iconButton">').click(function(e){
                    e.stopPropagation();
                    Dolphin.ajax({
                        url : thisPage.url.run,
                        pathData : {id : val},
                        onSuccess : function (reData) {
                            Dolphin.alert(reData.message);
                        }
                    });
                    return false;
                }).appendTo(content);

                $('<span class="glyphicon glyphicon-list-alt iconButton">').click(function(e){
                    e.stopPropagation();
                    thisPage.logList.query({job: val});
                    thisPage.logModal.modal('show');
                    return false;
                }).appendTo(content);

                return content;
            }
        }]
    });
    thisPage.logList = new Dolphin.LIST({
        panel: '#logList',
        checkbox: false,
        data: {rows:[], total:0},
        url: thisPage.url.log.list,
        columns: [{
            code: 'job.name',
            title: '任务',
        },{
            code: 'message',
            title: '日志',
        },{
            code: 'createTime',
            title: '时间',
        }]
    });


    thisPage.editModal = new Dolphin.modalWin({
        content : thisPage.editForm,
        title : "修改信息",
        defaultHidden : true,
        footer : $('#edit_form_footer'),
        hidden : function () {
            Dolphin.form.empty(thisPage.editForm);
        }
    });

    thisPage.detailModal = new Dolphin.modalWin({
        content : thisPage.detailForm,
        title : "查看详情",
        defaultHidden : true,
        hidden : function () {
            Dolphin.form.empty(thisPage.detailForm);
        }
    });

    thisPage.logModal = new Dolphin.modalWin({
        content : thisPage.logForm,
        title : "任务日志",
        defaultHidden : true,
        width: '1000px',
        hidden : function () {
            thisPage.logList.empty();
        }
    });
};


page.initEvent = function () {
    const thisPage = this;

    //查询
    thisPage.queryConditionForm.submit(function () {
        thisPage.list.query(Dolphin.form.getValue('queryConditionForm'));
        return false;
    });

    //新增
    $('#addData').click(function () {
        thisPage._id = "";
        thisPage.editModal.modal('show');
    });

    //修改
    $('#editData').click(function () {
        let checkedData = thisPage.list.getChecked();
        if(checkedData.length != 1){
            Dolphin.alert("请选择一条数据");
        }else{
            thisPage._id = checkedData[0]._id;
            Dolphin.form.setValue(checkedData[0], thisPage.editForm);
            thisPage.editModal.modal('show');
        }
    });

    //删除
    $('#removeData').click(function () {
        let checkedData = thisPage.list.getChecked();
        if(checkedData.length != 1){
            Dolphin.alert("请选择一条数据");
        }else{
            Dolphin.ajax({
                url : thisPage.url.remove,
                pathData : {id : checkedData[0]._id},
                onSuccess : function (reData) {
                    Dolphin.alert(reData.message, {
                        callback : function () {
                            thisPage.list.reload();
                        }
                    })
                }
            })
        }
    });

    //保存
    $('#edit_form_save').click(function () {
        let data = Dolphin.form.getValue("edit-form");
        Dolphin.ajax({
            url : thisPage.url.save,
            type : Dolphin.requestMethod.POST,
            data : Dolphin.json2string(data),
            pathData : {id : thisPage._id},
            onSuccess : function (reData) {
                Dolphin.alert(reData.message, {
                    callback : function () {
                        thisPage.editModal.modal('hide');
                        thisPage.list.reload();
                    }
                });
            }
        });
    });

    //导入
    $('#importData').fileupload({
        url: thisPage.url.importData,
        dataType: 'json',
        done: function (e, data) {
            Dolphin.alert(data.result.message, {
                callback: function () {
                    thisPage.list.reload();
                }
            })
        },
        progressall: function (e, data) {
            // console.log(data);
        }
    });
    //导出
    $('#exportDate').click(function () {
        window.open(thisPage.url.exportData + '?' + thisPage.queryConditionForm.serialize());
    });
};

page.showDetail = function (_id) {
    let thisPage = this;
    Dolphin.ajax({
        url : thisPage.url.detail,
        pathData : {id : _id},
        loading : true,
        onSuccess : function (reData) {
            Dolphin.form.setValue(reData.data, thisPage.detailForm, {
                formatter : {
                    createTime : function (val) {
                        return thisPage.formatterDate(val);
                    },
                    updateTime : function (val) {
                        return thisPage.formatterDate(val);
                    }
                }
            });
            thisPage.detailModal.modal('show');
        }
    })
};

page.formatterDate = function (val) {
    return Dolphin.date2string(new Date(Dolphin.string2date(val, "yyyy-MM-ddThh:mm:ss.").getTime() + 8 * 60 * 60 * 1000), "yyyy-MM-dd hh:mm:ss");
};


$(function () {
    page.init();
});