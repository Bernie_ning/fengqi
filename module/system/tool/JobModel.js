/**
 * Created by wangshuyi on 2016/12/27.
 */

'use strict';

/**
 * 文件管理
 */

const uuid = require('uuid');

const mongoose = require('../../util/mongoDB'),
    Schema = mongoose.Schema;
const config = require('../../../config/config');

const paramSchema = new Schema({
    key : { type: String},                         //key
    value : {type: String},                          //value
});

const schema = new Schema({
    _id : {type : String, default: uuid.v4},
    name : { type: String },                        //任务名称
    func : { type: String },                        //任务方法
    param : [paramSchema],                          //任务参数
    status : { type: String, default: 'stopped' },  //任务状态
    schedule : {type: String},                      //调度时间

    tenant : { type: String, ref : "M_Tenant", default: config.dbUser.admin.tenant },    //所属租户

    state : { type: Number, default : 1},           //是否有效
    createTime: {type: Date, default: Date.now},    //创建时间
    creater: {type: String, ref : "M_User", default: config.dbUser.robot._id},          //创建者
    updateTime : { type: Date, default: Date.now},  //最后更新时间
    updater : { type: String, ref : "M_User", default: config.dbUser.robot._id}         //最后更新者
});
schema.index({type: 1});
schema.index({lastUseTime: 1});

const model = mongoose.model('U_Job',schema);


module.exports = model;
