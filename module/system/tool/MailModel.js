/**
 * Created by wangshuyi on 2016/12/27.
 */

'use strict';

/**
 * 文件管理
 */

const uuid = require('uuid');

const mongoose = require('../../util/mongoDB'),
    Schema = mongoose.Schema;
const config = require('../../../config/config');
const tool = require('../../../module/util/tool');

const schema = new Schema({
    _id : {type : String, default: uuid.v4},
    from : { type: String },                        //发送源
    to : { type: String },                          //发送目标
    cc : { type: String },                          //抄送
    bcc : { type: String },                         //密送
    subject : { type: String },                     //发送主题
    content : { type: String },                     //发送内容
    attachments : { type: String },                 //发送内容
    status : {type: String},                        //发送状态
    result : {type: String},                        //发送结果

    type : { type : String},                        //业务类型

    tenant : { type: String, ref : "M_Tenant", default: config.dbUser.admin.tenant },    //所属租户
    state : { type: Number, default : 1},           //是否有效
    createTime: {type: Date, default: Date.now},    //创建时间
    creater: {type: String, ref : "M_User", default: config.dbUser.robot._id},          //创建者
    updateTime : { type: Date, default: Date.now},  //最后更新时间
    updater : { type: String, ref : "M_User", default: config.dbUser.robot._id}         //最后更新者
});
schema.path('createTime').get(function (v) {
    return tool.date2string(v, 'yyyy-MM-dd hh:mm:ss');
});
schema.set('toJSON', { getters: true});

const model = mongoose.model('U_Mail',schema);


module.exports = model;
