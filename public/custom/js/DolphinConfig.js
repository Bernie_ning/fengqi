"use strict";

Dolphin.systemConfig = {
    pageScript: "/page-script"
};
Dolphin.defaults.mockFlag = true;
Dolphin.path.contextPath = "";
Dolphin.path.publicPath = "";
Dolphin.path.uploadPath = Dolphin.path.publicPath + "/uploadFiles";
Dolphin.path.domain = "http://localhost:18080";

Dolphin.defaults.url.viewPrefix = "/view";
Dolphin.defaults.ajax.requestHeader = $.extend(Dolphin.defaults.ajax.requestHeader, {
    "ajax-flag": true
});
Dolphin.defaults.ajax.returnMsgKey = "message";
// Dolphin.defaults.modalWin.defaultHidden = true;
Dolphin.LIST.defaults.ajaxType = "post";
Dolphin.LIST.defaults.idField = "_id";
Dolphin.FORM.defaults.select.nameField = "text";
Dolphin.enum.setOptions({
    ajaxFlag: true,
    enumUrl: Dolphin.path.contextPath + "/system/dict/options/{id}",
    cookieFlag: false,
    textField: 'text'
});