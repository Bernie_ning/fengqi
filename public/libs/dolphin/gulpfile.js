/**
 * Created by wangshuyi on 2017/2/15.
 */

'use strict';

const gulp = require('gulp'),
    sass = require("gulp-sass");
const babel = require("gulp-babel");
const concat = require('gulp-concat');
const uglify = require("gulp-uglify");

gulp.task('sass', function () {
    return gulp.src('./sass/*')
        .pipe(sass().on('error', sass.logError))
        // .pipe(rename({suffix: '.min'}))   //rename压缩后的文件名
        // .pipe(cssnano())
        .pipe(gulp.dest('./dist/'));
});

gulp.task("script", function () {
    return gulp.src([
        'js/core.js',
        'js/i18n.js',
        'js/formI18nBox.js',
        'js/formFileBox.js',
        'js/validate.js',
        'js/form.js',
        'js/enum.js',
        'js/pagination.js',
        'js/list.js',
        'js/tree.js',
        'js/horizontalTree.js',
        'js/grid.js',
        'js/templateGrid.js',
        'js/refWin.js',
    ])
    .pipe(concat('dolphin.js'))
    .pipe(babel({
        presets: ['es2015']
    }))
    // .pipe(uglify())
    .pipe(gulp.dest("./dist/")); //转换成 ES5 存放的路径
});

gulp.task('watch', function () {
    gulp.watch([
        'js/core.js',
        'js/i18n.js',
        'js/formI18nBox.js',
        'js/formFileBox.js',
        'js/validate.js',
        'js/form.js',
        'js/enum.js',
        'js/pagination.js',
        'js/list.js',
        'js/tree.js',
        'js/horizontalTree.js',
        'js/grid.js',
        'js/templateGrid.js',
        'js/refWin.js',
    ], ['script']);
    gulp.watch('./sass/*.scss', ['sass']);
});

gulp.task('default', function() {
    // 将你的默认的任务代码放在这
    gulp.start('script', 'sass', 'watch');
});