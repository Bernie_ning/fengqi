/**
 * Created by Shubert.Wang on 2016/1/15.
 */
Dolphin.i18n.addMessages({
    //core
    core_modalWin_title : 'System Info',
    core_alert_title : 'System Info',
    core_confirm_title : 'System Info',
    core_prompt_title : 'System Info',
    core_jsonDate2string_error : 'Param Error: {1}',
    core_ajax_error : 'request server error',
    core_login_timeout : 'Login Timeout',
    core_reLogin : 'Please Refresh Page to Login.',
    core_alert_countDown : 'Auto Close After {1}s',
    core_confirm_yes : 'Yes',
    core_confirm_no : 'No',
    core_prompt_ok : 'OK',
    core_prompt_cancel : 'Cancel',

    //enum
    enum_cannot_found : "enumData({1}) not Found",

    //form
    "form.select.emptyOption": "-- Select --",

    //pagination
    "pagination.pageSize.start" : "Page Size: ",
    "pagination.pageSize.simple.start" : "Size: ",
    "pagination.pageSize.end" : "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",
    "pagination.curPage.from" : "Current: ",
    "pagination.curPage.to" : " - ",
    "pagination.curPage.end" : "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",
    "pagination.total.start" : "Total: ",
    "pagination.total.end" : " ",
}, 'en');

//core
Dolphin.defaults.modalWin.title = 'System Info';
Dolphin.defaults.alert.title = 'System Info';
Dolphin.defaults.confirm.title = 'System Info';
Dolphin.defaults.prompt.title = 'System Info';

//form
Dolphin.form.opts.select.pleaseSelect = "-- Select --";
// Dolphin.form.opts.select.nameField = "foreignText";

//enum
// Dolphin.enum.opts.textField = "foreignText";

//tree
// Dolphin.TREE.defaults.nameField = "foreignName";


