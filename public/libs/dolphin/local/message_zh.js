/**
 * Created by Shubert.Wang on 2016/1/15.
 */
Dolphin.i18n.addMessages({
    //core
    core_modalWin_title : '系统提示',
    core_alert_title : '系统提示',
    core_confirm_title : '系统提示',
    core_prompt_title : '系统提示',
    core_jsonDate2string_error : '参数({1})有误',
    core_ajax_error : '请求失败',
    core_login_timeout : '登录超时',
    core_reLogin : '请重新登录',
    core_alert_countDown : '{1}秒后自动关闭',
    core_confirm_yes : '确定',
    core_confirm_no : '取消',
    core_prompt_ok : '确定',
    core_prompt_cancel : '取消',

    //enum
    enum_cannot_found : "enumData({1})未找到。",

    //form
    "form.select.emptyOption": "--请选择--",

    //pagination
    "pagination.pageSize.start" : "每页显示",
    "pagination.pageSize.simple.start" : "每页",
    "pagination.pageSize.end" : "条，",
    "pagination.curPage.from" : "当前显示第 ",
    "pagination.curPage.to" : " 到 ",
    "pagination.curPage.end" : " 条，",
    "pagination.total.start" : "共 ",
    "pagination.total.end" : " 条记录。",
});

//core
Dolphin.defaults.modalWin.title = '系统提示';
Dolphin.defaults.alert.title = '系统提示';
Dolphin.defaults.confirm.title = '系统提示';
Dolphin.defaults.prompt.title = '系统提示';