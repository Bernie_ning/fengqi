/**
* Created by wangshuyi on 4/20/2017, 3:45:24 PM.
*/

'use strict';

var page = {
    editForm: $('#edit-form'),
    detailForm: $('#detail-form'),
    queryConditionForm: $('#queryConditionForm'),
    pageType: $('#pageType'),

    addArticleButton: $('#addArticleButton'),
    updateArticleButton: $('#updateArticleButton'),
    removeArticleButton: $('#removeArticleButton'),
    copyUrlButton: $('[id^=copyUrlButton]'),
    copyButton: $('#copys'),

    url: {
        list: '/article/bookcase/find',
        tree: '/article/bookcase/tree',
        remove: '/article/bookcase/remove/{id}',
        save: '/article/bookcase/save/{id}',
        detail: '/article/bookcase/detail/{id}',

        articleList: '/article/article/list',
        articleRemove: '/article/article/remove/{id}',
        file: {
            add: Dolphin.path.contextPath + '/system/tool/file/save',
            findIcon: Dolphin.path.contextPath + '/system/tool/file/find',
            findBanner: Dolphin.path.contextPath + '/article/bookcase/findBanner'
        }
    },

    _id: null,
    list: null,
    tree: null,
    editModal: null,

    init: null,
    initElement: null,
    initEvent: null,
    showDetail: null,
    formatterDate: null,
    toggleEditState: null
};

page.init = function () {
    page.initElement();
    page.initEvent();
};

page.initElement = function () {
    var thisPage = this;
    Dolphin.form.parse();
    var clipboard = new Clipboard('.btn');
    thisPage.tree = new Dolphin.TREE({
        panel: "#dataTree",
        url: thisPage.url.tree,
        idField: '_id',
        title: '文章树',
        multiple: false,
        onChecked: function onChecked(node) {
            var color = node.iconColor;
            var url = '' + Dolphin.path.domain + Dolphin.path.contextPath + '/fengqi/article' + node.pageType + '?bookcase=' + node._id;
            $('#catalogUrl').html('<a  style="width: 80%" target="_blank" href="' + url + '" class="textNowrap">' + url + '</a>\n<button type="button" class="btn btn-primary"  data-clipboard-text=\'' + url + '\'>\u590D\u5236\u94FE\u63A5</button>');
            if (node.pageType == '/bannerPage') {
                $('#bannerImg').show();
            } else {
                $('#bannerImg').hide();
            }
            if (!node.icon) {
                $('#showCatalogIcon').hide();
            } else {
                $('#showCatalogIcon').show();
            }
            Dolphin.ajax({
                url: thisPage.url.file.findIcon,
                type: 'get',
                data: { _id: node.icon },
                onSuccess: function onSuccess(reData) {
                    var icon = reData.rows[0].filePath;
                    $('#Icon').html('<div class="circleList" style="background-color: ' + color + ';top: 0px;position: absolute">\n                             <div class="weui-grid__icon " >\n                                 <img src=' + (Dolphin.path.uploadPath + icon) + ' alt="" id="show_img">\n                             </div>\n                        </div>');
                }
            });
            Dolphin.ajax({
                url: thisPage.url.file.findBanner,
                type: 'get',
                data: { ids: node.bannerImgs },
                onSuccess: function onSuccess(reData) {
                    $('.bannerImageBodys').remove();
                    var bannerImages = reData.data;
                    for (var i = 0; i < bannerImages.length; i++) {
                        if (bannerImages[i][0]) {
                            $('.bannerLists').append('<img src=' + (Dolphin.path.uploadPath + bannerImages[i][0].filePath) + ' class="bannerImageBodys" style="top:0px">');
                        }
                    }
                }
            });
            thisPage.addArticleButton.removeAttr('disabled');
            thisPage.addArticleButton.removeAttr('disabled');
            thisPage.copyButton.css("display", "block");
            thisPage.showDetail(node);
        },
        onLoad: function onLoad() {
            thisPage.addArticleButton.attr('disabled', 'disabled');
            thisPage.updateArticleButton.attr('disabled', 'disabled');
            thisPage.removeArticleButton.attr('disabled', 'disabled');
            if (thisPage._id) {
                this.check(this.findById(thisPage._id), true);
            }
        }
    });

    thisPage.list = new Dolphin.LIST({
        panel: "#dataList",
        url: thisPage.url.articleList,
        data: { rows: [] },
        pagination: true,
        multiple: false,
        // title : "文章列表",
        queryParams: Dolphin.form.getValue('queryConditionForm'),
        columns: [{
            code: "name",
            title: "标题"
        }, {
            code: "bookcase.name",
            title: "所属目录",
            width: '80px'
        }, {
            code: "author",
            title: "作者",
            width: '80px'
        }, {
            code: "time",
            title: "时间",
            formatter: function formatter(val) {
                return thisPage.formatterDate(val);
            }
        }, {
            code: "code",
            title: "地址",
            width: '350px',
            formatter: function formatter(val, row) {

                var div = $('<div> ');
                if (row.thirdUrl) {
                    $('<a id="a${row._id}" >').addClass('textNowrap').html('' + row.thirdUrl).attr('href', '' + row.thirdUrl).attr('target', '_black').appendTo(div);
                } else {
                    $('<a id="a${row._id}" class="textNowrap">').addClass('textNowrap').html('' + Dolphin.path.domain + Dolphin.path.contextPath + '/fengqi/article/articleView/' + row.code).attr('href', Dolphin.path.contextPath + '/view/article/articleMobileView?code=' + row.code).attr('target', '_black').appendTo(div);
                }
                return div;
            }
        }, {
            code: "copy",
            title: "复制链接",
            formatter: function formatter(val, row) {
                var div = $('<div>');
                var url = void 0;
                if (row.thirdUrl) {
                    url = row.thirdUrl;
                } else {
                    url = '' + Dolphin.path.domain + Dolphin.path.contextPath + '/fengqi/article/articleView/' + row.code;
                }
                $('<a>').addClass('').html('<button  type="button" class="btn btn-primary btn-xs"  data-clipboard-text=' + url + ' ">\u590D\u5236\u94FE\u63A5</button>').appendTo(div);
                return div;
            }
        }],
        onClick: function onClick(data) {
            thisPage.updateArticleButton.removeAttr('disabled');
            thisPage.removeArticleButton.removeAttr('disabled');
        },
        onLoadSuccess: function onLoadSuccess(data) {
            thisPage.updateArticleButton.attr('disabled', 'disabled');
            thisPage.removeArticleButton.attr('disabled', 'disabled');
        }
    });

    thisPage.editModal = new Dolphin.modalWin({
        content: thisPage.editForm,
        title: "修改信息",
        defaultHidden: true,
        width: '900px',
        footer: $('#edit_form_footer'),
        hidden: function hidden() {
            Dolphin.form.empty(thisPage.editForm);
        }
    });
};

page.initEvent = function () {
    var thisPage = this;

    //查询
    thisPage.queryConditionForm.submit(function () {
        thisPage.list.query(Dolphin.form.getValue('queryConditionForm'));
        return false;
    });
    thisPage.pageType.change(function () {
        if ($(this).val() == '/bannerPage') {
            $('.bannerPannel').show();
        } else {
            $('.bannerPannel').hide();
        }
    });

    //新增
    $('#addData').click(function () {
        $('.bannerPannel').hide();
        $('#editIcon').html('<div class="circleList">\n                <div class="weui-grid__icon " id="editIcon-grid">\n                    <img src="" alt="" id="seeImg">\n                </div>\n              </div>');
        thisPage._id = "";
        thisPage.editModal.modal('show');
        $('#addNewsItemButton').siblings().remove();
        $('.bannerImageBody').hide();
        $('#imageBody').hide();
    });

    //新增子节点
    $('#addChildrenData').click(function () {
        $('.bannerPannel').hide();
        $('.bannerImageBody').remove();
        $('#addNewsItemButton').siblings().remove();
        var checkedData = thisPage.tree.getChecked();
        if (checkedData.length != 1) {
            Dolphin.alert("请选择一条数据");
        } else {
            var data = {
                parent: checkedData[0]._id,
                parentName: checkedData[0].name
            };
            $('#editIcon').html('<div class="circleList">\n                <div class="weui-grid__icon " id="editIcon-grid">\n                    <img src="" alt="" id="seeImg">\n                </div>\n              </div>');
            thisPage._id = "";
            Dolphin.form.setValue(data, thisPage.editForm);
            thisPage.editModal.modal('show');
        }
    });

    //修改
    $('#editData').click(function () {
        var checkedData = thisPage.tree.getChecked();
        $('#addNewsItemButton').siblings().remove();
        if (checkedData.length != 1) {
            Dolphin.alert("请选择一条数据");
        } else {
            thisPage._id = checkedData[0]._id;
            var data = $.extend({}, checkedData[0]);
            if (data._parent) {
                data.parentName = data._parent.name;
                data.parent = data.parent._id;
            };
            if (data.pageType == '/bannerPage') {
                $('.bannerPannel').show();
            } else {
                $('.bannerPannel').hide();
            }
            Dolphin.ajax({
                url: thisPage.url.file.findBanner,
                type: 'get',
                data: { ids: data.bannerImgs },
                onSuccess: function onSuccess(reData) {
                    var bannerImages = reData.data;
                    $('.bannerImageBody').remove();
                    for (var i = 0; i < bannerImages.length; i++) {
                        if (bannerImages[i][0]) {
                            $('.bannerList').append('\n                            <span style="display: inline-block;position: relative" class=' + bannerImages[i][0]._id + '>\n                                <img src=' + (Dolphin.path.uploadPath + bannerImages[i][0].filePath) + ' class="bannerImageBody" style="top:-16px">\n                                <div id=' + bannerImages[i][0]._id + ' class="icon fa  fa-times-circle-o bannerClose" style="position: absolute;top: -15px;right: 5px;color: #b71e11" onclick="page.remove(this.id)"></div>\n                            </span>');
                        }
                    }
                }
            });
            if (checkedData[0].icon) {
                Dolphin.ajax({
                    url: thisPage.url.file.findIcon,
                    type: 'get',
                    data: { _id: checkedData[0].icon },
                    onSuccess: function onSuccess(reData) {
                        var icon = reData.rows[0].filePath;
                        $('#editIcon').html('<div class="circleList" style="background-color: ' + checkedData[0].iconColor + ';top: 0px;position: absolute">\n                             <div class="weui-grid__icon " id="editIcon-grid" >\n                                 <img src=' + (Dolphin.path.uploadPath + icon) + ' alt="" id="seeImg">\n                             </div>\n                    </div>');
                    }
                });
            } else {
                $('#iconColor').val('black');
                $('#editIcon').html('\n                 <div class="circleList">\n                     <div class="weui-grid__icon " id="editIcon-grid" >\n                         <img src="" alt="" id="seeImg">\n                     </div>\n                </div>\n            ');
            }

            Dolphin.form.setValue(data, thisPage.editForm);
            thisPage.editModal.modal('show');
        }
    });

    //删除
    $('#removeData').click(function () {
        var checkedData = thisPage.tree.getChecked();
        if (checkedData.length != 1) {
            Dolphin.alert("请选择一条数据");
        } else {
            Dolphin.ajax({
                url: thisPage.url.remove,
                pathData: { id: checkedData[0]._id },
                onSuccess: function onSuccess(reData) {
                    Dolphin.alert(reData.message, {
                        callback: function callback() {
                            thisPage._id = '';
                            thisPage.tree.reload();
                        }
                    });
                }
            });
        }
    });

    //保存
    $('#edit_form_save').click(function () {
        if (thisPage.pageType.val() != '/bannerPage') {
            $('#bannerImgs').val('');
        }
        var data = Dolphin.form.getValue("edit-form");
        Dolphin.ajax({
            url: thisPage.url.save,
            type: Dolphin.requestMethod.POST,
            data: Dolphin.json2string(data),
            pathData: { id: thisPage._id },
            onSuccess: function onSuccess(reData) {
                Dolphin.alert(reData.message, {
                    callback: function callback() {
                        thisPage._id = reData.data._id;
                        thisPage.tree.reload();
                        thisPage.showDetail(reData.data);
                        thisPage.editModal.modal('hide');
                    }
                });
            }
        });
    });

    //文章
    thisPage.addArticleButton.click(function () {
        var checkedData = thisPage.tree.getChecked();
        if (checkedData.length != 1) {
            Dolphin.alert('请选择一个目录');
        } else {
            Dolphin.goUrl('/article/articleEdit?bookcase=' + checkedData[0]._id);
        }
    });
    thisPage.updateArticleButton.click(function () {
        var checkedData = thisPage.tree.getChecked();
        var checkedList = thisPage.list.getChecked();
        if (checkedData.length != 1) {
            Dolphin.alert('请选择一个目录');
        } else if (checkedList.length != 1) {
            Dolphin.alert('请选择一条新闻');
        } else {
            Dolphin.goUrl('/article/articleEdit?bookcase=' + checkedData[0]._id + '&id=' + checkedList[0]._id + '&b_id=' + checkedList[0].bookcase._id);
        }
    });
    thisPage.removeArticleButton.click(function () {
        var checkedData = thisPage.list.getChecked();
        if (checkedData.length != 1) {
            Dolphin.alert("请选择一条数据");
        } else {
            Dolphin.confirm("确定要删除这条数据吗？", {
                callback: function callback(flag) {
                    if (flag) {
                        Dolphin.ajax({
                            url: thisPage.url.articleRemove,
                            pathData: { id: checkedData[0]._id },
                            onSuccess: function onSuccess(reData) {
                                Dolphin.alert(reData.message, {
                                    callback: function callback() {
                                        thisPage.list.reload();
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }
    });
    $('#fileupload').fileupload({
        url: thisPage.url.file.add + '?type=bookcaseIcon',
        dataType: 'json',
        done: function done(e, data) {
            $('#editIcon-grid').html('<img src=' + (Dolphin.path.uploadPath + data.result.data.filePath) + ' alt="" id="seeImg">');
            $("#seeImg").attr("src", Dolphin.path.uploadPath + data.result.data.filePath);
            $("#coverImage").val(data.result.data._id);
        }
    });
    $('#bannerUpload').fileupload({
        url: thisPage.url.file.add + '?type=bookcaseBanner',
        dataType: 'json',
        done: function done(e, data) {
            $('.bannerList').append('\n                  <span style="display: inline-block;position: relative" class=' + data.result.data._id + '>\n                        <img src=' + (Dolphin.path.uploadPath + data.result.data.filePath) + ' class="bannerImageBody" style="top:-16px">\n                        <div id=' + data.result.data._id + ' class="icon  fa  fa-times-circle-o bannerClose" style="" onclick="page.remove(this.id)"></div>\n                  </span>');

            // $(".bannerImageBody").attr("src", Dolphin.path.uploadPath+data.result.data.filePath).show();
            // $("#imageBody").attr("src", "/uploadFiles"+data.result.data.filePath).show();
            var val = void 0;
            if ($(".bannerImgs").val()) {
                val = $(".bannerImgs").val() + ',';
                val += data.result.data._id;
            } else {
                val = data.result.data._id;
            }

            $(".bannerImgs").val(val);
        }
    });
    $('#iconColor').change(function () {
        var color = $(this).val();
        $('.circleListDiv>.circleList').css({ background: color });
    });
};
page.remove = function (id) {
    var val = $('#bannerImgs').val();
    var valArr = val.split(',');
    valArr.forEach(function (value, index, arr) {
        if (value == id) {
            valArr.splice(index, 1);
        }
    });
    $('#bannerImgs').val(valArr.toString());
    $('.' + id).remove();
};
page.showDetail = function (node) {
    var thisPage = this;
    node.articalUrl = Dolphin.path.domain + '/fengqi/article/articleList?bookcasesId=' + node._id;
    thisPage.copyButton.attr("data-clipboard-text", '' + node.articalUrl);
    $('#articalUrl').html('' + node.articalUrl).attr('href', '' + node.articalUrl).attr('target', '_black');
    Dolphin.form.empty(thisPage.detailForm);
    Dolphin.form.setValue(node, thisPage.detailForm);
    thisPage.tree.expandTo(node);

    var bookcase = [];

    function allBookcase(_node) {
        bookcase.push(_node._id);
        if (_node.children) {
            _node.children.forEach(function (n) {
                allBookcase(n);
            });
        }
    }
    allBookcase(node);

    thisPage.list.query({ bookcase: { '$in': bookcase } });
};

page.formatterDate = function (val) {
    return Dolphin.date2string(new Date(Dolphin.string2date(val, "yyyy-MM-ddThh:mm:ss.").getTime() + 8 * 60 * 60 * 1000), "yyyy-MM-dd hh:mm:ss");
};
page.toggleEditState = function () {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'detail';
    var flag = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

    var thisPage = this;
    if (flag) {
        Dolphin.form.empty(thisPage.detailForm);
        Dolphin.form.empty(thisPage.editForm);
    }
    switch (state) {
        case 'edit':
            thisPage.detailForm.hide();
            thisPage.editForm.show();
            break;
        case 'detail':
            thisPage.detailForm.show();
            thisPage.editForm.hide();
            break;
    }
};

$(function () {
    Menu.select("Article");
    page.init();
});