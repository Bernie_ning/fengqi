/**
* Created by wangshuyi on 4/20/2017, 3:45:24 PM.
*/

'use strict';

var page = {
    url: {},

    _id: null,

    init: null,
    initElement: null,
    initEvent: null
};

page.init = function () {
    page.initElement();
    page.initEvent();
};

page.initElement = function () {
    var thisPage = this;
};

page.initEvent = function () {
    var thisPage = this;
};

$(function () {
    page.init();
});