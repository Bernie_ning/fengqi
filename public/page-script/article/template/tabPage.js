'use strict';

var pS = 20,
    pN = 1;
$('#page-infinite-navbar').infinite(60);
var loading = false; //状态标记

$(document).scroll(function () {
    console.log($(document).scrollTop() + window.innerHeight - $(document).height());
    if ($(document).scrollTop() + window.innerHeight - $(document).height() >= 0) {
        $('#pastLoad').show();
        if (loading) {
            $('#pastLoad').hide();
            return;
        } else {
            loading = true;
            Dolphin.ajax({
                url: Dolphin.path.contextPath + '/fengqi/article/tabPastForPage',
                data: { pageSize: pS, pageNumber: pN, bookcase: $('#bookcase').val() },
                onSuccess: function onSuccess(reData) {
                    $('#pastLoad').hide();
                    reData = reData.data.rows;
                    pN++;var filePath = void 0,
                        eft = void 0,
                        dat = void 0;
                    console.log(reData);
                    loading = false;
                    if (reData.length != 0) {
                        for (var i = 0; i < reData.length; i++) {
                            if (reData[i].img) {
                                filePath = reData[i].img.filePath;
                            }
                            if (reData[i].effectiveTime) {
                                eft = Dolphin.date2string(new Date(reData[i].effectiveTime), 'yyyy/MM/dd');
                            } else {
                                eft = '未规定时间';
                            }

                            if (reData[i].disActiveTime) {
                                dat = Dolphin.date2string(new Date(reData[i].disActiveTime), 'yyyy/MM/dd');
                            } else {
                                dat = '未规定时间';
                            }
                            $('.tab-news-container').append('\n                         <div class="tab-news-list">\n                            <a href="' + Dolphin.path.contextPath + '/fengqi/article/articleView/' + reData[i].code + '">\n                                <div class="tab-news-panel weui-flex">\n                                    <div class=" tab-news-img">\n                                        <img src="' + Dolphin.path.contextPath + '/uploadFiles' + filePath + '"\n                                             alt="">\n                                    </div>\n                                    <div class="tab-news-title ">\n                                        <p class="tab-title-font">' + reData[i].name + '</p>\n                                        <span class="tab-title-time">\n                                            <span class="icon fa fa-map-marker"></span>\n                                            ' + (reData[i].author || '地点未定') + '\n                                            <br><span class="icon fa fa-clock-o"></span>\n                                            ' + eft + '-' + dat + '\n                                    </div>\n                                </div>\n                            </a>\n                        </div>');
                        }
                    } else {
                        loading = true;
                        $('#haveAll').html('没有更多新闻了...');
                    }
                }
            });
        }
    }
});