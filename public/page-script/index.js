/**
 * Created by wangshuyi on 2017/2/4.
 */

'use strict';

var productListPage = {
    init: null,
    initElement: null,
    initEvent: null
};

productListPage.init = function () {
    this.initElement();
    this.initEvent();
};

productListPage.initElement = function () {
    var thisPage = this;
};
productListPage.initEvent = function () {
    var thisPage = this;
};

$(function () {
    productListPage.init();
});