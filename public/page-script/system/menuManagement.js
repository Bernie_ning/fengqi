/**
* Created by wangshuyi on 5/22/2017, 3:09:12 PM.
*/

'use strict';

var page = {
    editForm: $('#edit-form'),
    detailForm: $('#detail-form'),
    queryConditionForm: $('#queryConditionForm'),

    url: {
        list: '/system/menu/find',
        tree: '/system/menu/tree',
        remove: '/system/menu/remove/{id}',
        save: '/system/menu/save/{id}',
        detail: '/system/menu/detail/{id}',
        importData: Dolphin.path.contextPath + '/system/menu/import',
        exportData: '/system/menu/export'
    },

    _id: null,
    list: null,
    tree: null,
    editModal: null,

    init: null,
    initElement: null,
    initEvent: null,
    showDetail: null,
    formatterDate: null,
    toggleEditState: null
};

page.init = function () {
    page.initElement();
    page.initEvent();
};

page.initElement = function () {
    var thisPage = this;
    Dolphin.form.parse();

    thisPage.tree = new Dolphin.TREE({
        panel: "#dataTree",
        url: thisPage.url.tree,
        idField: '_id',
        title: '菜单树',
        multiple: false,
        onChecked: function onChecked(node) {
            thisPage.showDetail(node);
        },
        onLoad: function onLoad() {
            if (thisPage._id) {
                this.check(this.findById(thisPage._id), true);
            }
        }
    });

    thisPage.list = new Dolphin.LIST({
        panel: "#dataList",
        url: thisPage.url.list,
        data: { rows: [] },
        pagination: false,
        checkbox: false,
        title: "子菜单",
        queryParams: Dolphin.form.getValue('queryConditionForm'),
        columns: [{
            code: "name",
            title: "名称"
        }, {
            code: "code",
            title: "编码"
        }, {
            code: "sort",
            title: "排序"
        }]
    });

    thisPage.editModal = new Dolphin.modalWin({
        content: thisPage.editForm,
        title: "修改信息",
        defaultHidden: true,
        footer: $('#edit_form_footer'),
        hidden: function hidden() {
            Dolphin.form.empty(thisPage.editForm);
        }
    });
};

page.initEvent = function () {
    var thisPage = this;

    //查询
    thisPage.queryConditionForm.submit(function () {
        thisPage.list.query(Dolphin.form.getValue('queryConditionForm'));
        return false;
    });

    //新增
    $('#addData').click(function () {
        thisPage._id = "";
        thisPage.editModal.modal('show');
    });

    //新增子节点
    $('#addChildrenData').click(function () {
        var checkedData = thisPage.tree.getChecked();
        if (checkedData.length != 1) {
            Dolphin.alert("请选择一条数据");
        } else {
            var data = {
                parent: checkedData[0]._id,
                parentName: checkedData[0].name
            };
            thisPage._id = "";
            Dolphin.form.setValue(data, thisPage.editForm);
            thisPage.editModal.modal('show');
        }
    });

    //修改
    $('#editData').click(function () {
        var checkedData = thisPage.tree.getChecked();
        if (checkedData.length != 1) {
            Dolphin.alert("请选择一条数据");
        } else {
            thisPage._id = checkedData[0]._id;
            var data = $.extend({}, checkedData[0]);
            if (data._parent) {
                data.parentName = data._parent.name;
                data.parent = data.parent._id;
            }
            Dolphin.form.setValue(data, thisPage.editForm);
            thisPage.editModal.modal('show');
        }
    });

    //删除
    $('#removeData').click(function () {
        var checkedData = thisPage.tree.getChecked();
        if (checkedData.length != 1) {
            Dolphin.alert("请选择一条数据");
        } else {
            Dolphin.confirm("确定要删除这条数据吗？", {
                callback: function callback(flag) {
                    if (flag) {
                        Dolphin.ajax({
                            url: thisPage.url.remove,
                            pathData: { id: checkedData[0]._id },
                            onSuccess: function onSuccess(reData) {
                                Dolphin.alert(reData.message, {
                                    callback: function callback() {
                                        thisPage._id = '';
                                        thisPage.tree.reload();
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }
    });

    //保存
    $('#edit_form_save').click(function () {
        var data = Dolphin.form.getValue("edit-form");
        Dolphin.ajax({
            url: thisPage.url.save,
            type: Dolphin.requestMethod.POST,
            data: Dolphin.json2string(data),
            pathData: { id: thisPage._id },
            onSuccess: function onSuccess(reData) {
                Dolphin.alert(reData.message, {
                    callback: function callback() {
                        thisPage._id = reData.data._id;
                        thisPage.tree.reload();
                        thisPage.showDetail(reData.data._id);
                        thisPage.editModal.modal('hide');
                    }
                });
            }
        });
    });

    //导入
    $('#importData').fileupload({
        url: thisPage.url.importData,
        dataType: 'json',
        done: function done(e, data) {
            Dolphin.alert(data.result.message, {
                callback: function callback() {
                    thisPage.tree.reload();
                }
            });
        },
        progressall: function progressall(e, data) {
            // console.log(data);
        }
    });
    //导出
    $('#exportDate').click(function () {
        window.open(thisPage.url.exportData + '?' + thisPage.queryConditionForm.serialize());
    });
};

page.showDetail = function (node) {
    var thisPage = this;
    Dolphin.form.empty(thisPage.detailForm);
    Dolphin.form.setValue(node, thisPage.detailForm);
    thisPage.tree.expandTo(node);
    thisPage.list.query({ parent: node._id });
};

page.formatterDate = function (val) {
    return Dolphin.date2string(new Date(Dolphin.string2date(val, "yyyy-MM-ddThh:mm:ss.").getTime() + 8 * 60 * 60 * 1000), "yyyy-MM-dd hh:mm:ss");
};
page.toggleEditState = function () {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'detail';
    var flag = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

    var thisPage = this;
    if (flag) {
        Dolphin.form.empty(thisPage.detailForm);
        Dolphin.form.empty(thisPage.editForm);
    }
    switch (state) {
        case 'edit':
            thisPage.detailForm.hide();
            thisPage.editForm.show();
            break;
        case 'detail':
            thisPage.detailForm.show();
            thisPage.editForm.hide();
            break;
    }
};

$(function () {
    page.init();
});