/**
* Created by wangshuyi on 12/4/2017, 10:14:43 AM.
*/

'use strict';

var page = {
    editForm: $('#edit-form'),
    detailForm: $('#detail-form'),
    queryConditionForm: $('#queryConditionForm'),

    url: {
        list: '/system/tool/jobLog/list',
        remove: '/system/tool/jobLog/remove',
        save: '/system/tool/jobLog/save/{id}',
        detail: '/system/tool/jobLog/detail/{id}',
        importData: Dolphin.path.contextPath + '/system/tool/jobLog/import',
        exportData: '/system/tool/jobLog/export'
    },

    _id: null,
    list: null,
    editModal: null,
    detailModal: null,

    init: null,
    initElement: null,
    initEvent: null,
    showDetail: null
};

page.init = function () {
    page.initElement();
    page.initEvent();
};

page.initElement = function () {
    var thisPage = this;
    Dolphin.form.parse();

    thisPage.list = new Dolphin.LIST({
        panel: "#datalist",
        url: thisPage.url.list,
        title: "任务日志列表",
        queryParams: Dolphin.form.getValue('queryConditionForm'),
        columns: [{
            code: "job.name",
            title: "任务"
        }, {
            code: "message",
            title: "日志"

        }]
    });

    thisPage.editModal = new Dolphin.modalWin({
        content: thisPage.editForm,
        title: "修改信息",
        defaultHidden: true,
        footer: $('#edit_form_footer'),
        hidden: function hidden() {
            Dolphin.form.empty(thisPage.editForm);
        }
    });

    thisPage.detailModal = new Dolphin.modalWin({
        content: thisPage.detailForm,
        title: "查看详情",
        defaultHidden: true,
        hidden: function hidden() {
            Dolphin.form.empty(thisPage.detailForm);
        }
    });
};

page.initEvent = function () {
    var thisPage = this;

    //查询
    thisPage.queryConditionForm.submit(function () {
        thisPage.list.query(Dolphin.form.getValue('queryConditionForm'));
        return false;
    });

    //新增
    $('#addData').click(function () {
        thisPage._id = "";
        thisPage.editModal.modal('show');
    });

    //修改
    $('#editData').click(function () {
        var checkedData = thisPage.list.getChecked();
        if (checkedData.length != 1) {
            Dolphin.alert("请选择一条数据");
        } else {
            thisPage._id = checkedData[0]._id;
            Dolphin.form.setValue(checkedData[0], thisPage.editForm);
            thisPage.editModal.modal('show');
        }
    });

    //删除
    $('#removeData').click(function () {
        var checkedData = thisPage.list.getChecked(),
            ids = [];
        if (checkedData.length == 0) {
            Dolphin.alert("请至少选择一条数据");
        } else {
            checkedData.forEach(function (oa) {
                ids.push(oa._id);
            });

            Dolphin.confirm("确定要删除这些数据吗？", {
                callback: function callback(flag) {
                    if (flag) {
                        Dolphin.ajax({
                            url: thisPage.url.remove,
                            data: Dolphin.json2string({ ids: ids }),
                            type: Dolphin.requestMethod.POST,
                            onSuccess: function onSuccess(reData) {
                                Dolphin.alert(reData.message, {
                                    callback: function callback() {
                                        thisPage.editModal.modal('hide');
                                        thisPage.list.reload();
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }
    });

    //保存
    $('#edit_form_save').click(function () {
        var data = Dolphin.form.getValue("edit-form");
        Dolphin.ajax({
            url: thisPage.url.save,
            type: Dolphin.requestMethod.POST,
            data: Dolphin.json2string(data),
            pathData: { id: thisPage._id },
            onSuccess: function onSuccess(reData) {
                Dolphin.alert(reData.message, {
                    callback: function callback() {
                        thisPage.editModal.modal('hide');
                        thisPage.list.reload();
                    }
                });
            }
        });
    });

    //导入
    $('#importData').fileupload({
        url: thisPage.url.importData,
        dataType: 'json',
        done: function done(e, data) {
            Dolphin.alert(data.result.message, {
                callback: function callback() {
                    thisPage.list.reload();
                }
            });
        },
        progressall: function progressall(e, data) {
            // console.log(data);
        }
    });
    //导出
    $('#exportDate').click(function () {
        window.open(thisPage.url.exportData + '?' + thisPage.queryConditionForm.serialize());
    });
};

page.showDetail = function (_id) {
    var thisPage = this;
    Dolphin.ajax({
        url: thisPage.url.detail,
        pathData: { id: _id },
        loading: true,
        onSuccess: function onSuccess(reData) {
            Dolphin.form.setValue(reData.data, thisPage.detailForm, {
                formatter: {}
            });
            thisPage.detailModal.modal('show');
        }
    });
};

$(function () {
    page.init();
});