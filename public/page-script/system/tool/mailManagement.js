/**
* Created by wangshuyi on 11/13/2017, 5:19:40 PM.
*/

'use strict';

var page = {
    editForm: $('#edit-form'),
    detailForm: $('#detail-form'),
    queryConditionForm: $('#queryConditionForm'),

    url: {
        list: '/system/tool/mail/list',
        remove: '/system/tool/mail/remove',
        save: '/system/tool/mail/save/{id}',
        send: '/system/tool/mail/send',
        detail: '/system/tool/mail/detail/{id}',
        importData: Dolphin.path.contextPath + '/system/tool/mail/import',
        exportData: '/system/tool/mail/export'
    },

    _id: null,
    list: null,
    editModal: null,
    detailModal: null,

    init: null,
    initElement: null,
    initEvent: null,
    showDetail: null,
    formatterDate: null
};

page.init = function () {
    page.initElement();
    page.initEvent();
};

page.initElement = function () {
    var thisPage = this;
    Dolphin.form.parse();

    thisPage.list = new Dolphin.LIST({
        panel: "#datalist",
        url: thisPage.url.list,
        title: "邮件列表",
        queryParams: Dolphin.form.getValue('queryConditionForm'),
        columns: [{
            code: "subject",
            title: "标题",

            formatter: function formatter(val, row) {
                var link = $('<a href="javascript:void(0);">');
                link.click(function () {
                    thisPage.showDetail(row);
                }).html(val);
                return link;
            }
        }, {
            code: "from",
            title: "发件人"
        }, {
            code: "to",
            title: "收件人"
        }, {
            code: "type",
            title: "类型",
            formatter: function formatter(val) {
                return Dolphin.enum.getEnumText('mailType', val);
            }
        }, {
            code: "createTime",
            title: "发送时间"
        }, {
            code: "status",
            title: "状态",
            formatter: function formatter(val) {
                return Dolphin.enum.getEnumText('mailStatus', val);
            }
        }, {
            code: "result",
            title: "结果"
        }, {
            code: "_id",
            title: " ",
            formatter: function formatter(val, row) {
                var content = $('<div>');
                $('<span class="glyphicon glyphicon-send listButton">').attr('title', '重新发送').click(function () {
                    var data = Object.assign({}, row);
                    delete data._id;
                    delete data.createTime;
                    delete data.creater;
                    delete data.updateTime;
                    delete data.updater;
                    Dolphin.ajax({
                        url: thisPage.url.send,
                        type: Dolphin.requestMethod.POST,
                        data: Dolphin.json2string(data),
                        onSuccess: function onSuccess(reData) {
                            Dolphin.alert(reData.message, {
                                callback: function callback() {
                                    thisPage.list.reload();
                                }
                            });
                        }
                    });
                }).appendTo(content);
                return content;
            }
        }]
    });

    thisPage.editModal = new Dolphin.modalWin({
        content: thisPage.editForm,
        title: "新邮件",
        defaultHidden: true,
        footer: $('#edit_form_footer'),
        width: '1000px',
        hidden: function hidden() {
            Dolphin.form.empty(thisPage.editForm);
        }
    });

    thisPage.detailModal = new Dolphin.modalWin({
        content: thisPage.detailForm,
        title: "邮件内容",
        defaultHidden: true,
        width: '1000px',
        hidden: function hidden() {
            Dolphin.form.empty(thisPage.detailForm);
        }
    });
};

page.initEvent = function () {
    var thisPage = this;

    //查询
    thisPage.queryConditionForm.submit(function () {
        thisPage.list.query(Dolphin.form.getValue('queryConditionForm'));
        return false;
    });

    //新增
    $('#addData').click(function () {
        thisPage._id = "";
        thisPage.editModal.modal('show');
    });

    //保存
    $('#edit_form_save').click(function () {
        var data = Dolphin.form.getValue("edit-form");
        Dolphin.ajax({
            url: thisPage.url.send,
            type: Dolphin.requestMethod.POST,
            data: Dolphin.json2string(data),
            onSuccess: function onSuccess(reData) {
                Dolphin.alert(reData.message, {
                    callback: function callback() {
                        thisPage.editModal.modal('hide');
                        thisPage.list.reload();
                    }
                });
            }
        });
    });

    //导出
    $('#exportDate').click(function () {
        window.open(thisPage.url.exportData + '?' + thisPage.queryConditionForm.serialize());
    });
};

page.showDetail = function (data) {
    var thisPage = this;
    Dolphin.form.setValue(data, thisPage.detailForm);
    thisPage.detailModal.modal('show');
};

$(function () {
    page.init();
});