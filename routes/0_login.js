'use strict';
const express = require('express');
const router = express.Router();
const logger = require('log4js').getLogger("login");

const MenuService = require('../service/system/MenuService');
const UserService = require('../service/system/auth/UserService');
const roleConfig = require("../config/roleConfig");

const resUtil = require("../module/util/resUtil");

/* GET home page. */
router.get('/', function(req, res, next) {
    if(req.session.userData){
        res.redirect('/');
    }else{
        req.endType = "";
        // req.endType = global.tool.endType(req.headers['user-agent']);

        let url = 'common/login';

        res.render(url, {
            path : "/"+url,
            endType : "",
            data : {},
            body : {},
            userData:{},
            cookie : req.cookies,
            redirect : req.query.redirect
        });
    }
});

router.post('/', function(req, res, next){
    UserService.check({}, req.body.username, req.body.password).then(user => {
        UserService.login(req, user).then(
            s => res.send(resUtil.success(s))
        );
    }, e => res.send(resUtil.error(e)) );
});

router.get('/register', function(req, res, next){
    if(req.session.userData){
        res.redirect('/');
    }else{
        req.endType = "";
        // req.endType = global.tool.endType(req.headers['user-agent']);

        let url = 'common/register';

        res.render(url, {
            path : "/"+url,
            endType : "",
            data : {},
            body : {},
            userData:{},
            cookie : req.cookies,
            redirect : req.query.redirect
        });
    }
});
router.post('/register', function(req, res, next){
    data.role = roleConfig[1]._id;
    UserService.register({}, req.body).then(user => {
        UserService.login(req, user).then(
            s => res.send(resUtil.success(s))
        );
    }, e => res.send(resUtil.error(e)) );
});

router.get('/logout', function(req, res, next){
    req.session.destroy(function(err) {
        res.redirect(global.config.path.contextPath + '/');
    });
});

module.exports = router;
