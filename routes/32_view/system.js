
'use strict';
const express = require('express');
const router = express.Router();

router.get('*', function(req, res, next) {
    req.viewParam = {__curMenu:'sysMgr'};

    next();
});

/* GET home page. */
router.get('/tool/jobManagement', function(req, res, next) {
    req.viewParam = Object.assign(req.viewParam, {jobFunc: global.Job.method});

    next();
});

module.exports = router;
