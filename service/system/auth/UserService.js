/**
* Created by wangshuyi on 5/22/2017, 3:19:31 PM.
*/

'use strict';

const extend = require('extend');
const logger = require('log4js').getLogger("sys");

const CommonService = require("../../../service/common/dbService");
const Model = require("../../../module/system/auth/UserModel");
const MenuService = require("../../../service/system/MenuService");
const RoleService = require("../../../service/system/auth/RoleService");

const defaultParams = {
    model : Model,
    findCondition: (curUser, thisService) => {
        let condition = { state: 1 };
        return condition;
    },
    saveExtend: (curUser, thisService) => {
        return {}
    }
};

class ModuleService extends CommonService{
    constructor(param){
        super(param);
        this.opts = extend(true, {}, this.opts, defaultParams, param);
        this._name = "UserService";
    }

    /**
     *
     * @param curUser
     * @param code
     * @param password
     * @return {Promise}
     */
    check(curUser, code, password){
        return new Promise((resolve, reject) => {
            this.findOne(curUser, {code: code}, 'role').then(user => {
                if(user){
                    if(user.password === password){
                        resolve(user)
                    }else{
                        reject({message: '密码不正确'});
                    }
                }else{
                    reject({message: '该用户不存在'});
                }
            });
        });
    }
    login(req, user){
        return new Promise((resolve, reject) => {
            MenuService.tree(req.curUser, '', {_id: {'$in': user.role.menus}}).then(menus => {
                req.session.userData = {
                    _id : user._id,
                    name: user.name,
                    code: user.code,
                    org: user.org,
                    role : user.role,
                    tenant : user.tenant,
                    menu : menus
                };

                resolve({message: '登陆成功', data:req.session.userData});
            });
        });
    }

    /**
     *
     * @param curUser
     * @param data
     * @return {Promise}
     */
    register(curUser, data){
        return new Promise((resolve, reject) => {
            this.save(curUser, data).then(user => {
                RoleService.populate(curUser, user, {path:'role'}).then(s => {
                    resolve(user)
                });
            }, err => reject(err))
        });
    }
}

module.exports = new ModuleService();


