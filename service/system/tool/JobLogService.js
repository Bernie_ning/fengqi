/**
* Created by wangshuyi on 12/4/2017, 10:14:43 AM.
*/

'use strict';

const extend = require('extend');
const logger = require('log4js').getLogger("sys");

const CommonService = require("../../../service/common/dbService");
const Model = require("../../../module/system/tool/JobLogModel");

const defaultParams = {
    model : Model
};

class ModuleService extends CommonService{
    constructor(param){
        super(param);
        this.opts = extend(true, {}, this.opts, defaultParams, param);
        this._name = "U_JobLogService";
    }
}

module.exports = new ModuleService();


