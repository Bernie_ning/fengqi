/**
* Created by wangshuyi on 11/13/2017, 5:19:40 PM.
*/

'use strict';

const extend = require('extend');
const logger = require('log4js').getLogger("sys");

const CommonService = require("../../../service/common/dbService");
const Model = require("../../../module/system/tool/MailModel");

const Mail = require("../../../module/util/Mail");

const defaultParams = {
    model : Model
};

class ModuleService extends CommonService{
    constructor(param){
        super(param);
        this.opts = extend(true, {}, this.opts, defaultParams, param);
        this._name = "U_FileService";
    }

    sendMail(curUser, data){
        return new Promise((resolve, reject) => {
            data.status = 'sending';
            data.from = Mail.opts.auth.user;
            this.save(curUser, data).then(mail => {
                resolve(mail);
                Mail.sendMail(data).then(info => {
                    let updateData = {
                        status : 'success',
                        result : info.response,
                    };
                    this.updateById(curUser, mail._id, updateData).then(ok => {});

                }, err => {
                    let updateData = {
                        status : 'failed',
                        result : err.message,
                    };
                    this.updateById(curUser, mail._id, updateData).then(ok => {});
                });
            }, err => reject(err));
        });
    }
}

module.exports = new ModuleService();


